import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.alien.enterpriseRFID.tags.Tag;

public class RFIDTagListener
{
	private Connection connection;
	private final String url = "jdbc:postgresql://localhost/rfid_grooming_development";
	private final String user = "appdev";
	private final String password = "Dhsappdev123";

	public RFIDTagListener() throws SQLException
	{
		connection = DriverManager.getConnection(url, user, password);
	}

	public void tagListUpdate(Tag[] list, long readTime)
	{
		System.out.println(list.length + " tags");

		String readTimeString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(readTime));

		for (Tag tag : list)
		{
			System.out.println(tag.getTagID());
			
			String tagSerial = tag.getTagID().replaceAll(" ", "").trim();
			int antennaId = 0; // TODO: Get antenna id from database
			try
			{
				String fetchSqlString = "SELECT * FROM tags WHERE tag='" + tagSerial + "';";
				System.out.println(fetchSqlString);
				
				PreparedStatement fetchTagId = connection.prepareStatement(fetchSqlString);
				ResultSet fetchResult = fetchTagId.executeQuery();
				if (fetchResult.next())
				{
					int tagId = fetchResult.getInt("id");

					String sqlString = "INSERT INTO readings (time, antenna_id, tag_id, created_at, updated_at) VALUES (TIMESTAMP '" + readTimeString + "', " + antennaId + ", " + tagId + ", TIMESTAMP '" + readTimeString + "', TIMESTAMP '" + readTimeString + "');";
					System.out.println(sqlString);
					PreparedStatement insertReading = connection.prepareStatement(sqlString);
					insertReading.execute();
				}

			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}

	}
}