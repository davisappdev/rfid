import com.alien.enterpriseRFID.reader.AlienClass1Reader;
import com.alien.enterpriseRFID.tags.Tag;

public class Main
{
	public static void main(String[] args) throws Exception
	{
		AlienClass1Reader reader;

		NetworkDiscovery discoveredReader = new NetworkDiscovery();
		// Blocks the main thread until we get the reader
		reader = discoveredReader.getReader();
		System.out.println("Found reader!");

		// Sets credentials to connect to reader
		reader.setUsername("alien");
		reader.setPassword("password");

		reader.close(); // Keep this! It is necessary for reading to work!
		reader.open();

		reader.clearTagList();
		reader.setAntennaSequence("0 1 2");

		RFIDTagListener listener = new RFIDTagListener();

		System.out.println("Listening service started");

		// Block main thread until exit is typed in console
		while (true)
		{
			Tag[] tags = reader.getTagList();

			if (tags == null)
			{
				tags = new Tag[0];
			}

			listener.tagListUpdate(tags, System.currentTimeMillis());
			//sleep for one second
		}
	}
}