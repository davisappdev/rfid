package com.rfid.experiment;

import com.alien.enterpriseRFID.reader.AlienClass1Reader;
import com.alien.enterpriseRFID.tags.Tag;
import com.rfid.ui.TagAssignmentUI;
import com.rfid.utils.NetworkDiscovery;

public class ExperimentSetup
{
	private volatile AlienClass1Reader reader;
	private volatile RFIDTagListener tagListener;
	private volatile TagAssignmentUI tagAssignmentUI;

	public ExperimentSetup() throws Exception
	{
		tagAssignmentUI = new TagAssignmentUI();
		asyncStartReader();
	}

	public synchronized void asyncStartReader()
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					NetworkDiscovery discoveredReader = new NetworkDiscovery();
					// Blocks the main thread until we get the reader
					reader = discoveredReader.getReader();
					System.out.println("Found reader!");

					// Sets credentials to connect to reader
					reader.setUsername("alien");
					reader.setPassword("password");

					reader.close(); // Keep this! It is necessary for reading to work!
					reader.open();

					reader.clearTagList();
					reader.setAntennaSequence("0 1 2");

					RFIDTagListener listener = new RFIDTagListener();

					System.out.println("Listening service started");

					// Block main thread until exit is typed in console
					Tag[] tags = reader.getTagList();

					if (tags == null)
					{
						tags = new Tag[0];
					}
					else
					{
						tagAssignmentUI.tagDetected(tags[0].getTagID());
					}

					listener.tagListUpdate(tags, System.currentTimeMillis());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}

			}
		}).start();
	}
}
