package com.rfid.utils;

import org.json.JSONObject;

/**
 * 
 * @author Davis App Dev The class is intended solely to make de-serializing tag data pulled from a database easier.
 */
public class DeserializedTag
{
	private String alias, serial;

	public DeserializedTag(String alias, String serial)
	{
		this.alias = alias;
		this.serial = serial;
	}

	public DeserializedTag(JSONObject serializedData)
	{
		this.serial = serializedData.optString("serial");
		this.alias = serializedData.optString("name");
	}

	public String getAlias()
	{
		return alias;
	}

	public void setAlias(String alias)
	{
		this.alias = alias;
	}

	public String getSerial()
	{
		return serial;
	}

	public void setSerial(String serial)
	{
		this.serial = serial;
	}

	@Override
	public String toString()
	{
		return alias;
	}
}
