package com.rfid.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class NetworkUtils
{
	/**
	 * Executes a request and returns the response.
	 * 
	 * @param url
	 * @return
	 */
	public static String executeRequest(final String url)
	{
		try
		{
			URL request = new URL(url);
			BufferedReader reader = new BufferedReader(new InputStreamReader(request.openStream()));
			return getReaderContents(reader);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static String getReaderContents(BufferedReader reader)
	{
		String output = "";
		String t = "";
		try
		{
			while ((t = reader.readLine()) != null)
			{
				output += t;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return output;
	}
}
