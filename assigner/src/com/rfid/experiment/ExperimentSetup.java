package com.rfid.experiment;

import com.alien.enterpriseRFID.reader.AlienClass1Reader;
import com.alien.enterpriseRFID.tags.Tag;
import com.rfid.ui.TagAssignmentUI;

public class ExperimentSetup
{
	private AlienClass1Reader reader;
	private RFIDTagListener tagListener;
	private TagAssignmentUI tagAssignmentUI;

	public ExperimentSetup() throws Exception
	{
		tagAssignmentUI = new TagAssignmentUI();

		// NetworkDiscovery discoveredReader = new NetworkDiscovery();
		// // Blocks the main thread until we get the reader
		// reader = discoveredReader.getReader();
		// System.out.println("Found reader!");
		//
		// // Sets credentials to connect to reader
		// reader.setUsername("alien");
		// reader.setPassword("password");
		//
		// reader.close(); // Keep this! It is necessary for reading to work!
		// reader.open();
		//
		// reader.clearTagList();
		// reader.setAntennaSequence("0 1 2");
		//
		// RFIDTagListener listener = new RFIDTagListener();
		//
		// System.out.println("Listening service started");

		// Block main thread until exit is typed in console
		// Tag[] tags = reader.getTagList();
		Tag[] tags = null;

		if (tags == null)
		{
			tags = new Tag[0];
		}
		else
		{
			tagAssignmentUI.tagDetected(tags[0].getTagID());
		}

		// listener.tagListUpdate(tags, System.currentTimeMillis());
	}
}
