/**
 * 
 */
package edu.ucdavis.edu.SDKExamples;

import com.alien.enterpriseRFID.discovery.*;

/**
 * @author marvelez
 *
 */
public class NetworkDiscoveryTest implements DiscoveryListener {

	
	public NetworkDiscoveryTest() throws Exception {
	  NetworkDiscoveryListenerService service = new NetworkDiscoveryListenerService();
	  service.setDiscoveryListener(this);
	  service.startService();

	  // Spin while readers are discovered.
	  while (service.isRunning()) {
	    Thread.sleep(100);
	  }
	}
	
	/* (non-Javadoc)
	 * @see com.alien.enterpriseRFID.discovery.DiscoveryListener#readerAdded(com.alien.enterpriseRFID.discovery.DiscoveryItem)
	 */
	@Override
	public void readerAdded(DiscoveryItem discoveryItem) {
		System.out.println("Reader Added:\n" + discoveryItem.toString());	
	}

	/* (non-Javadoc)
	 * @see com.alien.enterpriseRFID.discovery.DiscoveryListener#readerRemoved(com.alien.enterpriseRFID.discovery.DiscoveryItem)
	 */
	@Override
	public void readerRemoved(DiscoveryItem discoveryItem) {
		System.out.println("Reader Removed:\n" + discoveryItem.toString());
	}

	/* (non-Javadoc)
	 * @see com.alien.enterpriseRFID.discovery.DiscoveryListener#readerRenewed(com.alien.enterpriseRFID.discovery.DiscoveryItem)
	 */
	@Override
	public void readerRenewed(DiscoveryItem discoveryItem) {
		System.out.println("Reader Renewed:\n" + discoveryItem.toString());
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new NetworkDiscoveryTest();
		} catch (Exception e) {
			System.out.println("Error:" + e.toString());
		}
	}

}
