/**
 * I connected the reader to the port 2 of the router.
 * I connected the computer to port 3 of the router.
 * I had to turn on DHCP for my lab computer where I normally have a fixed IP
 * address.
 * 
 * 
 * This example does not work yet.  It does not work because I am developing
 * on a machine with a fixed IP address that it not reachable by the router 
 * somehow.
 */
package edu.ucdavis.edu.SDKExamples;


import com.alien.enterpriseRFID.notify.Message;
import com.alien.enterpriseRFID.notify.MessageListener;
import com.alien.enterpriseRFID.notify.MessageListenerService;
import com.alien.enterpriseRFID.reader.AlienClass1Reader;
import com.alien.enterpriseRFID.tags.Tag;




/**
 * @author marvelez
 *
 */
public class MessageListenerTest implements MessageListener {

	/**
	 * Constructor.
	 */
	public MessageListenerTest() throws Exception {
	  // Set up the message listener service
	  MessageListenerService service = new MessageListenerService(4000);
	  service.setMessageListener(this);
	  service.startService();
	  System.out.println("Message Listener has Started");

	  Discoverer discoverer = new Discoverer();
	  AlienClass1Reader reader = discoverer.getReader();
	  reader.setUsername("alien");
	  reader.setPassword("password");
	  reader.open();
	  reader.setAntennaSequence("0 1");

	  //System.out.println(reader.getInfo());
	  
	  // Instantiate a new reader object, and open a connection to it on COM1
	  //AlienClass1Reader reader = new AlienClass1Reader("COM1");
	  //reader.open();
	  //System.out.println("Configuring Reader");


	  // Set up Notification.
	  // Use this host's IPAddress, and the port number that the service is listening on.
	  // getHostAddress() may find a wrong (wireless) Ethernet interface, so you may
	  // need to substitute your computers IP address manually.
	  System.out.println("Configuring Reader");

	  //System.out.println(reader.getInfo());

	  
	  //System.out.println(InetAddress.getLocalHost().getHostAddress());
	  System.out.println("192.168.1.100");
	  System.out.println(service.getListenerPort());
	  reader.setNotifyAddress("192.168.1.100", service.getListenerPort());
	  reader.setNotifyFormat(AlienClass1Reader.XML_FORMAT); // Make sure service can decode it.
	  reader.setNotifyTrigger("TrueFalse"); // Notify whether there's a tag or not
	  reader.setNotifyMode(AlienClass1Reader.ON);
	  
	  System.out.println("0: RF Attenuation = " + reader.getRFAttenuation(0));
	  System.out.println("1: RF Attenuation = " + reader.getRFAttenuation(1));
	  
	  reader.autoModeReset();// Set up AutoMode
	  
	  /**
	   * Settings to be manually set by us during tuning phase.
	   * 
	   * Let RIG be Reader Interface Guide.
	   * 
	   * AutoMode: See page 12 of RIG.
	   * Attenuation: See page 45 of RIG.
	   * Acquire Command: See page 93 of RIG.  Inventory vs Global Scroll
	   * TagListAntennaCombine: See page 83 of RIG.
	   * 
	   * 
	   */
	  //values range from 0 - 150.  A value of 30 decreases power by 3 DB.
	  reader.setRFAttenuation(0,50);
	  reader.setRFAttenuation(1,50);
	  reader.setRFAttenuation(2,50);
	  reader.setRFAttenuation(3,50);
	  // Globall Scroll only reads one tag and ignores all others.
	  reader.setAcquireMode(AlienClass1Reader.INVENTORY);
	  // We want to know which antennas are reading better and which ones to combine.
	  reader.setTagListAntennaCombine(AlienClass1Reader.OFF);
	  // We want the reader to read for 1 second.  Then report.
	  reader.setAutoStopTimer(1000);
	  reader.setAutoMode(AlienClass1Reader.ON); //START!
	  // We do not want old tags in the TagList table.
	  reader.clearTagList();
	  
	  
	  // Close the connection and spin while messages arrive
	  reader.close();
	  long runTime = 120000; // milliseconds
	  long startTime = System.currentTimeMillis();
	  do {
	    Thread.sleep(1000);
	  } while(service.isRunning() && (System.currentTimeMillis()-startTime) < runTime);
	  
	  // Reconnect to the reader and turn off AutoMode and TagStreamMode.
	  System.out.println("\nResetting Reader");
	  reader.open();
	  reader.autoModeReset();
	  reader.setNotifyMode(AlienClass1Reader.OFF);
	  reader.close();
	}
	
	/* (non-Javadoc)
	 * @see com.alien.enterpriseRFID.notify.MessageListener#messageReceived(com.alien.enterpriseRFID.notify.Message)
	 */
	@Override
	public void messageReceived(Message message) {
		System.out.println("\nMessage Received:");
		//System.out.println(message.getRawData());
		  if (message.getTagCount() == 0) {
		    System.out.println("(No Tags)");
		  } else {
		    for (int i = 0; i < message.getTagCount(); i++) {
		      Tag tag = message.getTag(i);
		      System.out.println(tag.toLongString());
		    }
		  }
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		  try {
			    new MessageListenerTest();
			  } catch (Exception e) {
			    System.out.println("Error:" + e.toString());
			  }
	}

}
