require 'test_helper'

class PowerReadingsControllerTest < ActionController::TestCase
  setup do
    @power_reading = power_readings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:power_readings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create power_reading" do
    assert_difference('PowerReading.count') do
      post :create, power_reading: { pen: @power_reading.pen, power: @power_reading.power, time: @power_reading.time }
    end

    assert_redirected_to power_reading_path(assigns(:power_reading))
  end

  test "should show power_reading" do
    get :show, id: @power_reading
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @power_reading
    assert_response :success
  end

  test "should update power_reading" do
    patch :update, id: @power_reading, power_reading: { pen: @power_reading.pen, power: @power_reading.power, time: @power_reading.time }
    assert_redirected_to power_reading_path(assigns(:power_reading))
  end

  test "should destroy power_reading" do
    assert_difference('PowerReading.count', -1) do
      delete :destroy, id: @power_reading
    end

    assert_redirected_to power_readings_path
  end
end
