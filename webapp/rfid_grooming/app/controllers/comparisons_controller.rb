
class ComparisonsController < ApplicationController
  before_action :set_comparison, only: [:show, :destroy]

	def index
		#@comparisons = Comparison.paginate(:page => params[:page], :per_page => 10)
		@comparisons = Comparison.all
		#respond_to do |format|
		#	format.html
		#	format.json {render json: @comparisons}
		#	format.js
		#end
	end


	def show
		@seconds = @comparison.comparison.paginate(:page => params[:page], :per_page => 100) 
		
    respond_to do |format|                                                          
      format.html                                                                   
      format.csv { send_data Comparison.to_csv(@comparison.id) }
    end 
	end


	def upload
		bout_sizes = [params[:b1].to_i, 
			params[:b2].to_i, 
			params[:b3].to_i,
			params[:b4].to_i,
			params[:b5].to_i,
			params[:b6].to_i,
			params[:b7].to_i,
			params[:b8].to_i]
		sandwich_sizes= [params[:s1].to_i, 
			params[:s2].to_i, 
			params[:s3].to_i,
			params[:s4].to_i,
			params[:s5].to_i,
			params[:s6].to_i,
			params[:s7].to_i,
			params[:s8].to_i]
		ComparisonsWorker.perform_async(params[:uploaded_file].original_filename, params[:uploaded_file].read, params[:tag_positions], bout_sizes, sandwich_sizes)
	end


  # DELETE /comparisons/1
  # DELETE /comparisons/1.json
  def destroy
    @comparison.destroy
    respond_to do |format|
      format.html { redirect_to comparisons_url }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comparison
      @comparison = Comparison.find(params[:id])
    end

end
