class IntervalsController < ApplicationController

  def index
		params[:cow_id] ||= 1
		# If params[:gap] is nil set it to 10, otherwise convert it to int.
		params[:gap] ||= 1
		@max_gap_size = params[:gap].to_i	
			
		if params["from"].nil? or params["to"].nil?
			@from_date = (Reading.order("time DESC").first.time - 1.day).at_beginning_of_day
			@to_date = Reading.order("time DESC").first.time.at_end_of_day
		else 
			@from_date = DateTime.new(params["from"]["(1i)"].to_i,
				params["from"]["(2i)"].to_i,
				params["from"]["(3i)"].to_i,
				params["from"]["(4i)"].to_i,
				params["from"]["(5i)"].to_i)
			@to_date = DateTime.new(params["to"]["(1i)"].to_i,
				params["to"]["(2i)"].to_i,
				params["to"]["(3i)"].to_i,
				params["to"]["(4i)"].to_i,
				params["to"]["(5i)"].to_i)
		end # for if-else

		if params["antennas"].nil? 
			@antennas = [0, 1, 2, 3]
		else
			@antennas = params["antennas"].map!{ |id_str| id_str.to_i}
		end 

		@cow = Cow.find(params[:cow_id].to_i)
		@cow.set_intervals(@max_gap_size, @from_date, @to_date, @antennas)

		@records = []
		@cow.intervals.each do |position, intervals|
			intervals.each do |interval|
				@records << [@cow.name, @antennas, position, interval]
			end
		end
		puts @records.size
		@records = @records.paginate(:page => params[:page], :per_page => 100)	
	
    respond_to do |format|
			format.html
			format.csv { send_data Cow.to_csv(:max_gap_size => @max_gap_size, :from_date => @from_date, :to_date => @to_date, :antennas => @antennas)  }
		end 
  end # def - index


end # def - controller
