class PowerReadingsController < ApplicationController
  before_action :set_power_reading, only: [:show, :edit, :update, :destroy]

  # GET /power_readings
  # GET /power_readings.json
  def index
 		@search = PowerReading.search(params[:q])                                            
    @power_readings = @search.result.order('time DESC').paginate(:page => params[:page])
  end

  # GET /power_readings/1
  # GET /power_readings/1.json
  def show
  end

  # GET /power_readings/new
  def new
    @power_reading = PowerReading.new
  end

  # GET /power_readings/1/edit
  def edit
  end

  # POST /power_readings
  # POST /power_readings.json
  def create
    @power_reading = PowerReading.new(power_reading_params)

    respond_to do |format|
      if @power_reading.save
        format.html { redirect_to @power_reading, notice: 'Power reading was successfully created.' }
        format.json { render action: 'show', status: :created, location: @power_reading }
      else
        format.html { render action: 'new' }
        format.json { render json: @power_reading.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /power_readings/1
  # PATCH/PUT /power_readings/1.json
  def update
    respond_to do |format|
      if @power_reading.update(power_reading_params)
        format.html { redirect_to @power_reading, notice: 'Power reading was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @power_reading.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /power_readings/1
  # DELETE /power_readings/1.json
  def destroy
    @power_reading.destroy
    respond_to do |format|
      format.html { redirect_to power_readings_url }
      format.json { head :no_content }
    end
  end


	def upload
		PowerReadingsWorker.perform_async(params[:uploaded_file].original_filename, params[:uploaded_file].read, params[:pen].to_i)
	end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_power_reading
      @power_reading = PowerReading.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def power_reading_params
      params.require(:power_reading).permit(:time, :power, :pen)
    end
end
