json.array!(@readings) do |reading|
  json.extract! reading, :id, :time, :tag_epc, :antenna
  json.url reading_url(reading, format: :json)
end
