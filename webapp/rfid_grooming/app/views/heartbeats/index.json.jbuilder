json.array!(@heartbeats) do |heartbeat|
  json.extract! heartbeat, :id, :pen, :time
  json.url heartbeat_url(heartbeat, format: :json)
end
