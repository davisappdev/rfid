class User < ActiveRecord::Base
  def self.from_omniauth(auth)
		if authorized?(auth.info.email)
			where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
			#where("email = '#{auth.info.email}'").first_or_initialize.tap do |user|
				user.provider = auth.provider
				user.uid = auth.uid
				user.name = auth.info.name
				user.email = auth.info.email
				user.oauth_token = auth.credentials.token
				user.oauth_expires_at = Time.at(auth.credentials.expires_at)
				user.save!
			end
		end
  end
	
	# For now, we assume a user is authorized if he/she is in the users table.
	# Otherwise, they are not authorized.	
	# In the future, we want to add an authorized column so that users can be
	# deactivated but their information kept for logging purposes.
	def self.authorized?(email)
		user = User.find_by(:email => email)
		return !user.nil? 
	end
end
