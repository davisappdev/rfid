class Tag < ActiveRecord::Base
	belongs_to :cow
	# The readings table has a tag_epc column, compare that with the tag's epc
	has_many :readings, :foreign_key => :tag_epc, :primary_key => :epc

	self.inheritance_column = nil

	def latest_reading
		return Reading.where(:tag_epc => self.epc).order("time DESC").first
	end

	def select_option
		"#{id} - #{epc}"
	end
end
