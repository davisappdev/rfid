class Reading < ActiveRecord::Base
	belongs_to :tag, :foreign_key => :tag_epc, :primary_key => :epc

	self.per_page = 100

  def self.to_csv()
    CSV.generate do |csv|                                                       
    csv << ["ID", "Time", "EPC", "Antenna", "Antenna Attenuation"]
      all.each do |reading|
        csv << [reading.id, reading.time, reading.tag_epc, reading.antenna, reading.antenna_attenuation]
      end # End of  all loop
    end
  end
end
