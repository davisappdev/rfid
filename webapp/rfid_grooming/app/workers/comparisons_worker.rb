# @animals STRUCTURE
# animal -- indexed by name ->
# 0) name
# observation_time_frame: 
# 1) start_time
# 2) end_time
# 3) video_obs{} -- sorted, for ordered display
## 0) start_time
## 1) end_time
## 2) video_behavior
## 3) video_brush_spinning 
# 4) rfid_readings{} -- indexed by time 
## reading array = [ tag_type, tag_position, antenna, antenna_attenuation] 
# 5) energy_readings{} -- indexed by time	
# 6) rfid_readings_bout_1{} -- indexed by time
## 1 or 0 
class ComparisonsWorker
	include Sidekiq::Worker
	sidekiq_options :retry => false

	def perform(csv_filename, csv_contents, tag_positions_filter, bout_sizes, sandwich_sizes)
	  puts "comparison = Comparison.new"
    @comparison = Comparison.new
    puts "comparison.filename = csv_filename"
    @comparison.filename = csv_filename
    puts "comparison.comparison = [header]"  
    @comparison.comparison = [header(bout_sizes, sandwich_sizes)]

		@animals, @bad_animals, @errors, @energy_readings = {}, [], [], {1 => {}, 2 => {}}
		# TODO: get all rfid readings for each pen
		#@reader_activity = {1 => {}, 2 => {}}

		puts "parse_csv_contents(csv_contents)"
		parse_csv_contents(csv_contents)
		puts "get_rfid_readings()"
		get_rfid_readings()
		puts "get_energy_readings()"
		get_energy_readings()
		#puts "get_reader_activity_for_both_pens()"
		#get_reader_activity_for_both_pens()
		puts "build_observations(bout_sizes, sandwich_sizes)"
		build_observations(bout_sizes, sandwich_sizes)
		    
    puts "comparison.save"
    @comparison.save 
    puts "saved!"
  end # perform

	def parse_csv_contents(csv_contents)
		# Read entire file
		# Determine which cows and which dates
    rows = CSV.parse(csv_contents, :skip_blanks => true)
    rows.shift # skip header line
		observation_count = rows.size

    rows.each.with_index(1) do |row, i|
      puts "#{i}/#{observation_count}"
			process_obs(parse_row(row))
    end # each row
	end

	def parse_row(row)
		video_obs = {}
		video_obs["cow_name"] = row[0]
		date = Date.parse(row[1])
		video_obs["start_time"] = combine_date_and_time(date, pretend_pst_is_utc(row[2]))
		video_obs["end_time"] = combine_date_and_time(date, pretend_pst_is_utc(row[3]))
		video_obs["behavior"] = row[5]
		return video_obs
	end #parse_row


	# For each observation,
	# learn which cow it belongs to
	# learn the endpoints for the observation
	# store the video observation for this cow
	def process_obs(video_obs)
		cow_name = video_obs["cow_name"]
		start_time = video_obs["start_time"]
		end_time = video_obs["end_time"]
		bad_animal = false
		new_animal = false

		# For each cow, 
		# get earliest start time
		# get latest end time 
		# Which cow is this observation for?
		if !@bad_animals.include?(cow_name)
			# if not seen yet
			if !@animals.has_key?(cow_name)
				cow = Cow.find_by(:name => cow_name)
				# name can be incorrect cow may be nil
				if cow.nil?
					bad_animal = true
					@errors << "Cow #{cow_name} does not exist."
					puts "ERROR: Cow #{cow_name} does not exist."
					@bad_animals << cow_name
				else
					new_animal = true
					# Initialize
					@animals[cow_name] = [cow, start_time, end_time] 	
					# Initialize video observations 
					@animals[cow_name][3] = []
					# Initialize RFID readings
					@animals[cow_name][4] = {}
				end
			end

			# cow already seen, update time frame endpoints
			if !bad_animal
				if !new_animal
					@animals[cow_name][1] = start_time if start_time < @animals[cow_name][1] 
					@animals[cow_name][2] = end_time if end_time > @animals[cow_name][2] 
				end

				# store video behavior
				## start_time, end_time, behavior, video_brush_spinning
				video_obs = [start_time, end_time, video_obs["behavior"], 
					is_spinning?(video_obs["behavior"]) ? 1 : 0]
				@animals[cow_name][3] << video_obs
			end

		end
	end


	def get_rfid_readings
		# For each cow,
		# for each of its tags 
		# store a "reading" array
		# = [ tag_type, tag_position, antenna, antenna_attenuation] 
		# The 4th element of each value in the animals hash is a hash index by time	
		# TODO: tag_positions_filter	
		@animals.each do |name, data|
			# Get all readings for this cow within the observation timeframe
			start_time = @animals[name][1]
			end_time = @animals[name][2]
			@animals[name][0].tags.each do |tag|
				readings = Reading.where(:tag_epc => tag.epc, :time => start_time..end_time)
				readings.each do |reading|
					@animals[name][4][truncate_to_secs(reading.time)] = [tag.tag_type, 
						tag.position, reading.antenna, reading.antenna_attenuation] 
				end		
			end
		end
	end

	
	# TODO: review this code
	# TODO: get all cows for each pen
	# for each cow, get all of its readings
	# combine them into @reader_activity[cow.pen]
	def get_reader_activity_for_both_pens
		# TODO: review this code
		# TODO: limit to a certain time frame
		[1,2].each do |pen|
			cows = Cow.where(:pen => 1)
			cows.each do |cow|
				cow.tags.each do |tag|
					tag.readings.each do |reading|
						@reader_activity[cow.pen][reading.time] = true
					end
				end
			end
		end
	end


	def combine_date_and_time(date, time)   
		return Time.utc(date.year, date.month, date.day, time.hour, time.min, time.sec)     
	end # combine_date_and_time


	def is_spinning?(video_behavior)
		video_behavior.downcase!
		spinning_behaviors = ["head/neck_spincontact",
			"body_spincontact",
			"posterior_spincontact",
			"brush_vicinity_up_spin",
			"brush_vicinity_lie_no_contact_spin",
			"brush_vicinity_lie_contact_spin",
			"not_in_brush_vicinity_spin"]
		return spinning_behaviors.include?(video_behavior)
	end


	def get_energy_readings
		start_time, end_time = *find_energy_readings_time_range
		[1,2].each do |pen|
			sql = "pen = #{pen} AND power > 0 
				AND time BETWEEN date_trunc('minute', '#{start_time}'::timestamp) 
					AND date_trunc('minute', '#{end_time}'::timestamp)"
			readings = PowerReading.select("id, time, pen").where(sql).order("time")
			readings.each do |reading|
				@energy_readings[pen][reading.time] = true 
			end
		end
	end #get_energy_readings


	def find_energy_readings_time_range
		times = []
		@animals.each do |name, data|
			times << data[1]
			times << data[2]
		end
		times.sort!
		return times.first, times.last
	end


	# return 1 if there is a rfid reading within the last bout_size seconds
	# start with the current time
	# for example, rfid reading at 7:13:22, bout size == 10, means that
	# 7:13:22 -- 7:13:31 will all be 1
	def rfid_bout(cow_name, time, bout_size)
		i = 0
		while i <= (bout_size - 1)
			return 1 if @animals[cow_name][4].has_key?(time - i)
			i = i + 1
		end	
		return 0
	end


	# return 1 if 
	def rfid_sandwich(cow_name, time, sandwich_size)
		return 1 if @animals[cow_name][4].has_key?(time)

		left_boundary = 0
		right_boundary = 0

		# look back n spaces
		i = 1
		while i <= sandwich_size
			if @animals[cow_name][4].has_key?(time - i)	
				left_boundary = 1 
				break
			end
			i = i + 1
		end
		
		# let m = number of spaces to look forward
		m = (sandwich_size + 1) - i

		# look forward m spaces
		j = 1
		while j <= m
			if @animals[cow_name][4].has_key?(time + j)
				right_boundary = 1 
				break
			end
			j = j + 1
		end
	
		return left_boundary && right_boundary		
	end




	def header(bout_sizes, sandwich_sizes)
		header = ["cow_name", "time", "behavior_video", "video_brush_spinning",
			"behavior_rfid", "tag_type", "tag_position", "antenna", 
			"antenna_attenuation", "energy_usage"]
		bout_sizes.each do |size|
			header << "bout_#{size}"	
		end
		sandwich_sizes.each do |size|
			header << "sandwich_#{size}"	
		end
		#header << "reader_active"
		return header
	end

	# build observations
	def build_observations(bout_sizes, sandwich_sizes)
		@animals.each do |name, data|
			data[3].each do |video_obs|
				time = video_obs[0] 
				while time < video_obs[1]
					observation = []
					observation << name
					observation << time  
					observation << video_obs[2] # video_behavior
					observation << video_obs[3] # video_spinning_brush
					if @animals[name][4].has_key?(time)
						observation << 1 # rfid_detected 
						observation << @animals[name][4][time][0] # tag_type
						observation << @animals[name][4][time][1] # tag_position
						observation << @animals[name][4][time][2] # antenna 
						observation << @animals[name][4][time][3] # antenna_attenuation
					else
						observation << 0 # rfid_detected 
						observation << "NA" # tag_type
						observation << "NA" # tag_position
						observation << "NA" # antenna 
						observation << "NA" # antenna_attenuation
					end

					if @energy_readings[@animals[name][0].pen].has_key?(truncate_to_mins(time))
						observation << 1 # energy_reading
					else
						observation << 0 # energy_reading
					end

					bout_sizes.each do |size|
						observation << rfid_bout(name, time, size) 	
					end	
	
					sandwich_sizes.each do |size|
						observation << rfid_sandwich(name, time, size)	
					end
				
					# Did the reader in this pen read at least one tag (including
					# this animal)?
					
					#begin
					#	if @reader_activity[@animals[name][0].pen].has_key?(time)
					#		observation << 1 # at least one tag was read in this pen
					#	else
					#		observation << 0 # no tag was read in this pen 
					#	end
					#rescue
					#	puts "@reader_activity.nil?= #{@reader_activity.nil?}"
					#	puts name	
					#	puts time 
					#	puts @animals[name].inspect	
					#	puts @animals[name][0].inspect	
					#	exit
					#end
					
					@comparison.comparison << observation
					time = time + 1
				end
			end
		end 
	end


	def truncate_to_mins(time) return time.change(:sec => 0) end
	def truncate_to_secs(time) return time.change(:usec => 0) end
	def pretend_pst_is_utc(time_str) return Time.parse(time_str + " UTC") end

end
