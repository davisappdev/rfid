class PowerReadingsWorker
  include Sidekiq::Worker
    sidekiq_options :retry => false

  def perform(csv_filename, csv_contents, pen)

		i = 0
		rows = CSV.parse(csv_contents)
		puts "#{rows.size} rows"
		rows.shift # ignore header
		rows.each do |row|
			reading = parse_row(row)
			if i % 1000 == 0
				puts i 
				puts row.inspect 
				puts reading.inspect
			end

			# check if there are power readings within the time for this pen
			energy_reading = PowerReading.where(time: reading["time"], pen: pen).first_or_initialize	
			energy_reading.power = reading["power"] 
			energy_reading.save

			i = i + 1
		end # each row

  end # perform

	def parse_row(row)
		reading = {}
		timestamp = Time.parse(row[0] + " UTC")
		#puts row[0]
		#timestamp = DateTime.strptime(row[0] + " UTC", "%m/%d/%Y %H:%M:%S")
		reading["time"] = timestamp
		if row[1].nil?
			reading["power"] = 0
		else
			reading["power"] = row[1]
		end
		return reading
	end #parse_row

end
