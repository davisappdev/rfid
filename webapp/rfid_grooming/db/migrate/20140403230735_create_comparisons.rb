class CreateComparisons < ActiveRecord::Migration
  def change
    create_table :comparisons do |t|
      t.text :comparison

      t.timestamps
    end
  end
end
