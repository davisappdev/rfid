class CreatePowerReadings < ActiveRecord::Migration
  def change
    create_table :power_readings do |t|
      t.timestamp :time
      t.decimal :power
      t.integer :pen

      t.timestamps
    end
  end
end
