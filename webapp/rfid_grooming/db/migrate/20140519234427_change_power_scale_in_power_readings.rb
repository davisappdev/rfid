class ChangePowerScaleInPowerReadings < ActiveRecord::Migration
  def change
		change_column :power_readings, :power, :integer
  end
end
