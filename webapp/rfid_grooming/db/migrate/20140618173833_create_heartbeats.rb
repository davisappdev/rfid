class CreateHeartbeats < ActiveRecord::Migration
  def change
    create_table :heartbeats do |t|
      t.integer :pen
      t.timestamp :time
    end
		execute "ALTER TABLE heartbeats ALTER time SET DEFAULT now()"
  end
end
