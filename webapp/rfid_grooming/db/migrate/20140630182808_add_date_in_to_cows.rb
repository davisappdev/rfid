class AddDateInToCows < ActiveRecord::Migration
  def change
    add_column :cows, :date_in, :timestamp
  end
end
