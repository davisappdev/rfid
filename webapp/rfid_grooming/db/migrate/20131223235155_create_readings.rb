class CreateReadings < ActiveRecord::Migration
  def change
    create_table :readings do |t|
      t.timestamp :time	
			t.string :tag_epc
      t.integer :antenna
		
			# We are alreay storing time so we do no need more timestamps.	
      #t.timestamps
    end
  end
end
