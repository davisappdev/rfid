class AddDateOutToCows < ActiveRecord::Migration
  def change
    add_column :cows, :date_out, :timestamp
  end
end
