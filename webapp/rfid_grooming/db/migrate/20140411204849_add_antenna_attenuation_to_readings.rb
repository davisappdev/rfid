class AddAntennaAttenuationToReadings < ActiveRecord::Migration
  def change
    add_column :readings, :antenna_attenuation, :integer
  end
end
