class RenameAntennaToAntennaIdInReadings < ActiveRecord::Migration
  def change
		rename_column :readings, :antenna, :antenna_id
  end
end
