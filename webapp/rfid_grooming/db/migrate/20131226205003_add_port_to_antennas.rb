class AddPortToAntennas < ActiveRecord::Migration
  def change
    add_column :antennas, :port, :integer
  end
end
