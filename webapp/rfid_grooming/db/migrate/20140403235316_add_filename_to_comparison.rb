class AddFilenameToComparison < ActiveRecord::Migration
  def change
    add_column :comparisons, :filename, :text
  end
end
