class AddAntennaReferenceToAntennaPlacements < ActiveRecord::Migration
  def change
    add_reference :antenna_placements, :antenna, index: true
  end
end
