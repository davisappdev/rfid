<?php

require_once("login_funcs.php");
require_once("tag_funcs.php");

if(!checkLogin()) {
	echo "2LOGIN KEY ERROR!!!";
	die;
}

if(isset($_REQUEST["serial"]) && isset($_REQUEST["name"])) {
	$override = false;
	if(isset($_REQUEST["override"]) && $_REQUEST["override"]) {
		$override = true;
	}

	echo setTag($_REQUEST["serial"], $_REQUEST["name"], $override);
} else {
	echo "2Error: serial and tag name not present in URL";
}


?>