# Tag Listener


## Description
This is a Java program that finds the Alien RFID Tag reader on the network and 
begins recording RFID tags detected in a database.  This program is meant to run 
as a daemon, i.e, without user interaction.


## Libraries
I contacted support for the Alien reader using their website.  They sent me an 
ftp link to their Java SDKs.  As of January 15, 2014, we are using the latest 
version of the Alien Java SDK.

###SDK Version:
v2.3.1 (January 2011)

###Source: 
ftp://ftp.alientechnology.com/pub/readers/software/sdk/java/
