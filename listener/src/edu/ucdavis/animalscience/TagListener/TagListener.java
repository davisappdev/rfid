/**
 * This program listens for tags and stores them in a PostgreSQL database table.  
 * It implements the MessageListener interface provided by the Alien SDK.
 * 
 * This program assumes that there is only one nearby Alien RFID reader. 
 * Once it finds the reader, it sets it to autonomous mode.
 * Specifically, it tells the reader to read tags for 1 second, then to send it
 * all of the tags it read.
 * 
 * The listener connects to reader, turns on automode.  In automode, the reader 
 * will read for one second, and send notifications (messages) to the listener.  
 * The listener's callback method will then simply parse the message and 
 * store (time, tag EPC, antenna) in a PostgreSQL database table.
 *  
 * Hardware Setup Notes:
 * Computer connected to port 1 of the router.
 * Reader connected to port 2 of the router.
 * I had to turn on DHCP for my lab computer where I normally have a fixed IP 
 * address.
 * 
 */
package edu.ucdavis.animalscience.TagListener;

import com.alien.enterpriseRFID.notify.Message;
import com.alien.enterpriseRFID.notify.MessageListener;
import com.alien.enterpriseRFID.notify.MessageListenerService;
import com.alien.enterpriseRFID.reader.AlienClass1Reader;
import com.alien.enterpriseRFID.reader.AlienReaderException;
import com.alien.enterpriseRFID.tags.Tag;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;


/**
 * @author marvelez
 *
 */
public class TagListener implements MessageListener {
	private AlienClass1Reader reader;
	private MessageListenerService service;
	private Connection connection_local;
	private Connection connection_remote;
	private static Properties props;
	private int[] attenuations;
	private final String sql = "INSERT INTO readings (time, tag_epc, antenna, antenna_attenuation) VALUES (?,?,?,?)";
	private PreparedStatement stmt_local;
	private PreparedStatement stmt_remote;
	private final String hearbeat_sql = "INSERT INTO heartbeats (pen) VALUES (?)";
	private PreparedStatement heartbeat_stmt_local;
	private PreparedStatement heartbeat_stmt_remote;
	private static int pen;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			props = new Properties();
			FileInputStream in;
			try {
				in = new FileInputStream("config.properties");
				props.load(in);
				in.close(); 
				pen = Integer.parseInt(props.getProperty("pen"));
				new TagListener();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();	
			}//try			
		} catch (Exception e) {
			System.out.println("Error:" + e.toString());
		}//try
	}//main
	
	
	/**
	 * Constructor.  We do the setup and listening here.  Why?  This is how the Alien
	 * example class was written.
	 */
	public TagListener() throws Exception {
		
		connect_to_local_database();
		if (connection_local == null){
			System.err.println("ERROR: Connection to local PostgresSQL failed!");
			System.exit(1);	
		} else {
			System.out.println("Local PostgreSQL connection succeeded!");
			System.out.println("Preparing SQL statement...");
			stmt_local = connection_local.prepareStatement(sql);
			heartbeat_stmt_local = connection_local.prepareStatement(hearbeat_sql);
			System.out.println("Prepared local statements.");
		}
		
		connect_to_remote_database();
		if (connection_remote == null){
			System.err.println("WARNING: Connection to remote PostgresSQL failed!");
			//System.exit(1);	
		} else {
			System.out.println("Remote PostgreSQL connection succeeded!");
			System.out.println("Preparing SQL statement...");
			stmt_remote = connection_remote.prepareStatement(sql);
			heartbeat_stmt_remote = connection_remote.prepareStatement(hearbeat_sql);
			System.out.println("Prepared remote statements.");
		}
		
		find_reader();
		if (reader == null){
			System.err.println("No reader was found!");
			System.exit(1);	
		}
		System.out.println("Found " + reader);
		System.out.println("Reader = " + reader.toString());
		System.out.println("Reader IP Address = " + reader.getIPAddress());
		System.out.println("Reader MAC = " + reader.getMACAddress());
		
		if (!set_notify_mode()) {
			System.err.println("Setting nofication mode failed!");
			System.exit(1);	
		}
		System.out.println("Notify mode is ON.");
		
		attenuations = new int[4];
		for(int i = 0; i <= 3; i++){
			attenuations[i] = Integer.parseInt(props.getProperty("attenuation_ant" + i));	
		}
		
		if (!configure_reader()) {
			System.err.println("Setting reader options failed!");
			System.exit(1);	
		}
		System.out.println("Configuration succeeded.");

		
		/**
		 * Establish this class as the listener.
		 */
		service = new MessageListenerService(Integer.parseInt(props.getProperty("notify_host_port")));
		service.setMessageListener(this);
		service.startService();
		System.out.println("Listening...");

		/**
		 * By now, the reader has been set to autonomous mode.  
		 * It will send messages to this application regularly.
		 * This listener will sleep for the same amount of time it instructed
		 * the reader to read, i.e., 1 second.
		 */
		try{
			while(service.isRunning()){
				heartbeat_stmt_local.setInt(1, pen);
				heartbeat_stmt_local.execute();
				heartbeat_stmt_remote.setInt(1, pen);
				heartbeat_stmt_remote.execute();
				Thread.sleep(1000);				
			}
		} catch(InterruptedException e) {
			/**
			 * Ideally, we should terminate this program using Ctrl+C at the 
			 * console.  That way we can reset the reader.  We want to turn off
			 * AutoMode.
			 */
			System.out.println("Resetting reader...");
			reader.open();
			reader.autoModeReset();
			System.out.println("autoModeReset()");
			reader.setNotifyMode(AlienClass1Reader.OFF);
			System.out.println("Notify Mode = OFF");
			reader.close();
			System.out.println("Reader has been reset!");
		}//try - catch
	}//constructor
	


	/* (non-Javadoc)
	 * @see com.alien.enterpriseRFID.notify.MessageListener#messageReceived(com.alien.enterpriseRFID.notify.Message)
	 */
	@Override
	public void messageReceived(Message message) {
		//System.out.println("\nMessage Received:");
		try {						
			if (message.getTagCount() != 0){
				for (int i = 0; i < message.getTagCount(); i++){
					Tag tag = message.getTag(i);
					int antenna = tag.getAntenna();
					Timestamp discovered_time = new Timestamp(tag.getDiscoverTime());
					String tag_epc = tag.getTagID();
					//check if time
					System.out.println(discovered_time + "\t" + tag + "\t" + antenna);
					//remote
					if(stmt_remote != null){
						stmt_remote.setTimestamp(1, discovered_time);
						stmt_remote.setString(2, tag_epc);
						stmt_remote.setInt(3, antenna);
						stmt_remote.setInt(4, attenuations[antenna]);
						stmt_remote.addBatch();	
					}
					
					//local - this should always succeed
					stmt_local.setTimestamp(1, discovered_time);
					stmt_local.setString(2, tag_epc);
					stmt_local.setInt(3, antenna);
					stmt_local.setInt(4, attenuations[antenna]);
					stmt_local.addBatch();					
				}//iterate over all tags
				stmt_local.executeBatch();
				if(stmt_remote != null){
					stmt_remote.executeBatch();
				} else {
					System.out.println("WARNING: There is no connection to remote database.");
				}
			}//if tags were read
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}//messageReceived

	
	/** 
	 * We rely on the SDK DiscoveryListener to automatically find the reader 
	 * that is connected to the same network.  We assume only reader is 
	 * connected.  We do not need to know its IP address.
	 * 
	 */
	private void find_reader(){
		System.out.println("Searching for reader...");
		Discoverer discoverer;
		try {
			discoverer = new Discoverer();
			reader = discoverer.getReader();
			reader.setUsername("alien");
			reader.setPassword("password");
			reader.open();
			reader.setAntennaSequence("0 1 2 3");	
		} catch (Exception e) {
			e.printStackTrace();
		}//try
	}//find_reader()

	
	/**
	 * This method connects to the PostgreSQL database.
	 * The credentials should be stored in a file, config.properties, which 
	 * should be in the root directory of this project.
	 * 
	 * Reminder:
	 * You may need to edit the postgresql.conf file.  It defaults to localhost.
	 * Ensure that the pg_hba.conf file has the following 
	 * line:
	 * host    all             all             127.0.0.1/32            md5
	 * 
	 */
	private void connect_to_local_database(){
		System.out.println("Connecting to local PostgreSQL database...");

		try {	
			connection_local = DriverManager.getConnection(
					props.getProperty("local_db_url"),
					props.getProperty("local_db_user"), 
					props.getProperty("local_db_password"));
		} catch (SQLException e) {			
			e.printStackTrace();
		}//try - catch

	}//connect_to_database
	
	private void connect_to_remote_database(){
		System.out.println("Connecting to remote PostgreSQL database...");

		Properties properties = new Properties();
		properties.setProperty("user", props.getProperty("remote_db_user"));
		properties.setProperty("password", props.getProperty("remote_db_password"));
		properties.setProperty("tcpKeepAlive", "true");
		try {	
			connection_remote = DriverManager.getConnection(props.getProperty("remote_db_url"), properties);
		} catch (SQLException e) {			
			e.printStackTrace();
		}//try - catch

	}//connect_to_database
	
	
	/**
	 * Before we turn on automode, thereby letting the reader run autonomously, 
	 * we must tell it to notify this listener.  We tell it to read for 1 
	 * second, and sends us the tags in XML format.
	 * 
	 * Technical Note:
	 * Ensure the the host address and port are correct.
	 * @return
	 */
	private boolean set_notify_mode(){
		/**
		 * Turn on notification.
		 * Set notification settings.
		 * The reader will send the taglist to this application.
		 */
		System.out.println("Setting reader to Notify mode...");
		//System.out.println("Host Address: 192.168.1.100"); //port 1 -?-> x.x.x.100
		//System.out.println("Host Port: 4000");
		try {
			/** 
			 * Connect computer to port 1 of the router.  Then it should get 
			 * the IP address: 192.168.1.100.  
			 */
			reader.setNotifyAddress(props.getProperty("notify_host_url"), 
					Integer.parseInt(props.getProperty("notify_host_port")));
			reader.setNotifyFormat(AlienClass1Reader.XML_FORMAT); // Make sure service can decode it.
			reader.setNotifyTrigger("TrueFalse"); // Notify whether there's a tag or not
			reader.setNotifyMode(AlienClass1Reader.ON);	
		} catch (AlienReaderException e) {		
			System.err.println("ERROR: The host address is probably wrong.");
			e.printStackTrace();
			return false;
		}//try
		return true;
	}//set_notify_mode()
	
	
	/**
	 * Manual reader settings.
	 * 
	 * Let RIG be Reader Interface Guide.
	 * 
	 * AutoMode: See page 12 of RIG.
	 * Attenuation: See page 45 of RIG.
	 * Acquire Command: See page 93 of RIG.  Inventory vs Global Scroll
	 * TagListAntennaCombine: See page 83 of RIG.
	 * 
	 */
	private boolean configure_reader(){
		System.out.println("Configuring reader...");

		try {
			reader.autoModeReset();//reset to defaults
		
			//Attenuation values range from 0 to 150.
			System.out.println("Attenutation Settings:");
			for(int i = 0; i <= 3; i++){
				reader.setRFAttenuation(i,attenuations[i]);
				System.out.println(i +": RF Attenuation = " + reader.getRFAttenuation(i));
			}
			
			// Global Scroll only reads one tag and ignores all others.
			reader.setAcquireMode(AlienClass1Reader.INVENTORY);
			// We want to know how well antennas are reading.
			reader.setTagListAntennaCombine(AlienClass1Reader.OFF);
			reader.setAutoStopTimer(1000);//Read for 1 second. Then report
			reader.clearTagList(); // no old tags in the TagList
			reader.setAutoMode(AlienClass1Reader.ON); //START!
			reader.close();
		} catch (AlienReaderException e) {
			e.printStackTrace();
			return false;
		}//try-catch
		return true;
	}//set_reader_options()

}//TagListener